import json
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import networkx as nx

TERM = '1208'


def pprint(*argv):
    for arg in argv:
        print(json.dumps(arg, sort_keys=True,
                         indent=4, separators=(',', ': ')))


def get_course_times(course_name):
    def get_timeslot(section_time):
        def get_min_time(time_str):
            h, m = time_str.split(":")
            m, isPM = int(m[:-2]), m[-2:] == "PM"
            h = int(h)
            if h != 12 and isPM:
                h += 12
            return (h * 60 + m) / 60
        return list(map(get_min_time, section_time))

    sub, cnum = course_name.split(" ")
    course = data[sub][cnum]

    times = []
    for sec, details in course['sections'].items():
        if (details['location'] not in ["To Be Announced", "Totally Online"]):
            days = set(list(details['days']))
            time = get_timeslot(details['time'].split(" - "))
            times.append((sec, days, time))

    return times


with open(f'{TERM}.json') as json_file:
    data = json.load(json_file)[TERM]

courses = ['CIST 2500', 'CSCI 2240', 'CIST 3000', 'JAPN 1110']
num_courses = len(courses)

# get course times
course_times = dict(zip(courses, map(get_course_times, courses)))

G = nx.Graph()

clist = list(course_times.items())
nodes = {}
node_colors = list(colors.get_named_colors_mapping().values())

# generate nodes
for course_name, course_secs in clist:
    course_nodes = []
    for sec_name, _, _ in course_secs:
        node_name = f'{course_name} {sec_name}'
        G.add_node(node_name)
        course_nodes.append(node_name)
    nodes[course_name] = course_nodes

# generate edges
for i in range(num_courses):
    for j in range(i+1, num_courses):
        c1_name, c1_secs = clist[i]
        c2_name, c2_secs = clist[j]
        for c1_sec_name, c1_sec_days, c1_sec_times in c1_secs:
            for c2_sec_name, c2_sec_days, c2_sec_times in c2_secs:
                common_days = list(c1_sec_days.intersection(c2_sec_days))
                c1_node_name = f'{c1_name} {c1_sec_name}'
                c2_node_name = f'{c2_name} {c2_sec_name}'
                if len(common_days) != 0:
                    c1_start, c1_end = c1_sec_times
                    c2_start, c2_end = c2_sec_times

                    if c1_start == c2_start or c1_start <= c2_end and c1_end >= c2_start:
                        G.add_edge(c1_node_name, c2_node_name)
                continue

# remove unnecessary nodes
invalid_nodes = []
for node in G.nodes:
    neighbors = list(G.neighbors(node))
    if len(neighbors) == 0:
        invalid_nodes.append(node)
        nodes[node[:-4]].remove(node)

for node in invalid_nodes:
    G.remove_node(node)

# print constraints
edges = [e for e in G.edges]
pprint(edges, f'{len(edges)} constraints found')

# plot w/ legend
pos = nx.spring_layout(G)
for node_group_index in range(num_courses):
    course_name, _ = clist[node_group_index]
    node_group = nodes[course_name]
    node_group_color = node_colors[node_group_index]
    nx.draw_networkx_nodes(G, pos,
                           nodelist=node_group,
                           node_color=node_group_color,
                           label=course_name)
nx.draw_networkx_edges(G, pos=pos)
nx.draw_networkx_labels(G, pos=pos, font_weight='bold')
plt.legend(scatterpoints=1)
plt.show()
