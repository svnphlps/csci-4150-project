import inflection
import json
import requests
import time

from bs4 import BeautifulSoup
from itertools import repeat
from multiprocessing import Pool

BASE_URL = "https://www.unomaha.edu/registrar/students/before-you-enroll/class-search/"

NUM_PROCESSES = 4


def camelize(str):
    return inflection.camelize(str.replace(" ", "_"), False)


def get_term_nums():
    page = requests.get(BASE_URL)
    soup = BeautifulSoup(page.text, features="lxml")
    term_options = soup.find('select').findAll('option')
    return [int(term_option['value']) for term_option in term_options]


def get_term_subject_nums(term):
    subjects = requests.get(f"{BASE_URL}subjects.load.php?term={term}").json()
    return [subject['value'] for subject in subjects]


def get_terms(terms, subjects=None):
    pool = Pool(processes=NUM_PROCESSES)

    term_data = {}
    for term in terms:
        if subjects is None:
            subjects = get_term_subject_nums(term)

        results = pool.starmap(get_term_subject, zip(repeat(term), subjects))
        term_data[term] = dict(zip(subjects, results))

    return term_data


def get_term_subject(term, subject):
    def unwrap_text(el):
        return el.text

    def unwrap_table(table_parent):
        return table_parent.find('table')

    def get_field_list(section, field_name, field_value):
        curr_field_value = section.get(field_name)
        curr_field_value = curr_field_value + \
            [field_value] if curr_field_value else [field_value]
        return (field_name, curr_field_value)

    page = requests.get(f"{BASE_URL}?term={term}&subject={subject}")
    soup = BeautifulSoup(page.text, features='lxml')

    courses_html = soup.findAll('div', {'class': 'dotted-bottom'})
    courses_html.pop(0)

    courses = {}
    for course_html in courses_html:
        course = {}

        course_info = list(map(unwrap_text, course_html.findAll('p')))
        course['name'] = course_info[0]
        course['desc'] = course_info[1]
        if len(course_info) > 2:
            course['prereq'] = course_info[2].replace("Prereq: ", "")

        sections = {}
        table_parents = course_html.findAll('div', {'class': 'col-md-6'})
        for table in map(unwrap_table, table_parents):
            section = {}

            for tr in table.findAll('tr', {'class': None}):
                tds = tr.findAll('td')

                field_name, field_value = camelize(tds[0].text), tds[1].text
                if field_name == "classNumber":
                    field_value = field_value.replace(" ?", "")
                elif field_name == "courseAttribute":
                    field_name, field_value = get_field_list(
                        section, "courseAttributes", field_value)
                elif field_name == "note":
                    field_name, field_value = get_field_list(
                        section, "notes", field_value)

                section[field_name] = field_value

            section_num = table.find('th').text.split(" ")[-1]
            sections[section_num] = section

        course['sections'] = sections

        course_num = course_html.find('h2').text.split(" ")[-1]
        courses[course_num] = course

    print(f"Term {term}: {len(courses)} {subject} course(s) found")

    return courses


def dump(json_dict):
    return json.dumps(json_dict, sort_keys=True, indent=4, separators=(',', ': '))


def main():
    json_data = dump(get_terms(get_term_nums()[-1:]))

    with open("1208.json", 'w') as f:
        f.write(json_data)


if __name__ == "__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % round(time.time() - start_time, 2))
