import json
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import networkx as nx

TERM = '1208'


def pprint(*argv):
    for arg in argv:
        print(json.dumps(arg, sort_keys=True,
                         indent=4, separators=(',', ': ')))


def get_course_times(course_name):
    def get_timeslot(section_time):
        def get_min_time(time_str):
            h, m = time_str.split(":")
            m, isPM = int(m[:-2]), m[-2:] == "PM"
            h = int(h)
            if h != 12 and isPM:
                h += 12
            return (h * 60 + m) / 60
        return list(map(get_min_time, section_time))

    sub, cnum = course_name.split(" ")
    course = data[sub][cnum]

    times = []
    for sec, details in course['sections'].items():
        if (details['location'] not in ["To Be Announced", "Totally Online"]):
            days = set(list(details['days']))
            time = get_timeslot(details['time'].split(" - "))
            times.append((sec, days, time))

    return times


# read scraped results
with open(f'{TERM}.json') as json_file:
    data = json.load(json_file)[TERM]

courses = ['CIST 2500', 'CSCI 2240', 'CIST 3000', 'JAPN 1110']
num_courses = len(courses)

# get course times
course_times = dict(zip(courses, map(get_course_times, courses)))

G = nx.Graph()

clist = list(course_times.items())
nodes = {}
colors = list(colors.get_named_colors_mapping().values())
edges = {'No days in common': [], 'Has days in common': []}

# generate nodes
for course_name, course_secs in clist:
    course_nodes = []
    for sec_name, _, _ in course_secs:
        node_name = f'{course_name} {sec_name}'
        G.add_node(node_name)
        course_nodes.append(node_name)
    nodes[course_name] = course_nodes

# generate edges
for i in range(num_courses):
    for j in range(i+1, num_courses):
        c1_name, c1_secs = clist[i]
        c2_name, c2_secs = clist[j]
        for c1_sec_name, c1_sec_days, c1_sec_times in c1_secs:
            for c2_sec_name, c2_sec_days, c2_sec_times in c2_secs:
                common_days = list(c1_sec_days.intersection(c2_sec_days))
                edge_type = 'No days in common'
                if len(common_days) != 0:
                    edge_type = 'Has days in common'
                    c1_start, c1_end = c1_sec_times
                    c2_start, c2_end = c2_sec_times

                    if c1_start == c2_start or c1_start <= c2_end and c1_end >= c2_start:
                        continue

                c1_node_name = f'{c1_name} {c1_sec_name}'
                c2_node_name = f'{c2_name} {c2_sec_name}'
                edge = (c1_node_name, c2_node_name)
                edges[edge_type].append(edge)
                G.add_edge(*edge)

# remove unnecessary nodes
invalid_nodes = []
for node in G.nodes:
    neighbors = list(G.neighbors(node))
    if len(set(map(lambda x: x[:-4], neighbors))) != num_courses - 1:
        invalid_nodes.append(node)

for node in invalid_nodes:
    G.remove_node(node)

# print schedules
cliqs = []
for clique in nx.find_cliques(G):
    cliq_list = list(clique)
    cliq_list.sort()
    if len(cliq_list) == num_courses:
        cliqs.append(cliq_list)

pprint(cliqs, f'{len(cliqs)} unique schedules found')

# plot w/ legend
pos = nx.spring_layout(G)
for node_group_index in range(num_courses):
    course_name, _ = clist[node_group_index]
    node_group = nodes[course_name]
    node_group_color = colors[node_group_index]
    nx.draw_networkx_nodes(G, pos,
                           nodelist=node_group,
                           node_color=node_group_color,
                           label=course_name)
for index, edge_type_tupe in enumerate(edges.items()):
    edge_type, edges = edge_type_tupe
    edge_colors = ['r', 'b']
    nx.draw_networkx_edges(G, pos=pos, edgelist=edges,
                           edge_color=edge_colors[index],
                           label=edge_type)
nx.draw_networkx_labels(G, pos=pos, font_weight='bold')
plt.legend(scatterpoints=1)
plt.show()
